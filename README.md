# GPU PCI Express benchmark

This benchmark demonstrates various methods of transfering data from host to device and from device to host.

To build and run the benchmark on a CUDA device, use the following steps:

```sh
git clone https://gitlab.com/astron-misc/cuda-pcie.git
cmake -S. -Bbuild
make -Cbuild
./build/main
```

This assumes that CUDA is installed and that it can be found by CMake. Other dependencies (`cudawrappers` and `cxxopts`) are fetched automatically.

The benchmark is also compatible with HIP and can be built and ran on AMD GPUs as follows:

```sh
git clone https://gitlab.com/astron-misc/cuda-pcie.git
cmake -S. -Bbuild -DBUILD_WITH_HIP=1
make -Cbuild
./build/main
```

This assumes that an AMD GPU driver and ROCm are installed. Moreover, the ROCm related binaries should be in your `PATH`, so that CMake can detect HIP, e.g.: `PATH=$PATH:/opt/rocm/bin`.
Also note that this relies on an experimental branch of `cudawrappers`.

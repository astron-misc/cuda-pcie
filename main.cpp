#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <iostream>

#include <cudawrappers/cu.hpp>
#include <cxxopts.hpp>

#define DATA_LENGTH static_cast<size_t>(1024 * 1e6)
#define DATA_SIZE DATA_LENGTH * sizeof(float)

std::ostream &operator<<(std::ostream &os, cu::Device &device) {
  os << device.getName() << std::endl;
  size_t device_memory = device.totalMem(); // Bytes
  const int shared_memory = device.getAttribute<
      CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK>(); // Bytes
  const int clock_frequency =
      device.getAttribute<CU_DEVICE_ATTRIBUTE_CLOCK_RATE>() * 1e-3; // Mhz
#if defined(__HIP__)
  const int mem_frequency =
      device.getAttribute<CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE>() *
      2e-3; // MHz
#else
  const int mem_frequency =
      device.getAttribute<CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE>() / 4e3; // MHz
#endif
  const int nr_sm =
      device.getAttribute<CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT>();
  const int mem_bus_width =
      device
          .getAttribute<CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH>(); // Bits
  os << "\tArchitecture  : " << device.getArch() << std::endl;
  os << "\tDevice memory : " << round(device_memory / (1024. * 1024 * 1024))
     << " Gb" << std::endl;
  os << "\tShared memory : " << shared_memory / 1024 << " Kb" << std::endl;
  os << "\tClk frequency : " << clock_frequency << " Mhz" << std::endl;
  os << "\tMem frequency : " << mem_frequency << " Mhz" << std::endl;
  os << "\tNumber of SMs : " << nr_sm << std::endl;
  os << "\tMem bus width : " << mem_bus_width << " bit" << std::endl;
  os << "\tMem bandwidth : " << mem_bus_width * mem_frequency / 1000 << " GB/s"
     << std::endl;
  os << std::endl;
  return os;
}

void report(double seconds, long bytes) {
  float bandwidth = bytes / seconds * 1e-9;
  std::cout << std::setw(5) << seconds << " s, " << bandwidth << " GB/s";
}

void copy_data(std::string name, cu::Stream &stream, cu::DeviceMemory &d_data,
               void *h_data, int nr_repetitions = 1) {
  cu::Event start, stop;

  std::cout << ">> " << name << " (copy):" << std::endl;
  for (int i = 0; i < nr_repetitions; i++) {
    // HtoD
    stream.record(start);
    stream.memcpyHtoDAsync(d_data, h_data, d_data.size());
    stream.record(stop);
    stream.synchronize();
    const double runtime_htod = stop.elapsedTime(start) * 1e-3;

    // DtoH
    stream.record(start);
    stream.memcpyDtoHAsync(h_data, d_data, d_data.size());
    stream.record(stop);
    stream.synchronize();
    double runtime_dtoh = stop.elapsedTime(start) * 1e-3;

    std::cout << "HtoD: ";
    report(runtime_htod, DATA_SIZE);
    std::cout << ", DtoH: ";
    report(runtime_dtoh, DATA_SIZE);
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

void prefetch_data(std::string name, cu::Device &device, cu::Stream &stream,
                   cu::DeviceMemory &d_data, int nr_repetitions = 1) {
  cu::Event start, stop;

  std::cout << ">> " << name << " (prefetch):" << std::endl;
  for (int i = 0; i < nr_repetitions; i++) {
    // HtoD
    stream.record(start);
    stream.memPrefetchAsync(d_data, d_data.size(), device);
    stream.record(stop);
    stream.synchronize();
    const double runtime_htod = stop.elapsedTime(start) * 1e-3;

    // DtoH
    stream.record(start);
    stream.memPrefetchAsync(d_data, d_data.size());
    stream.record(stop);
    stream.synchronize();
    double runtime_dtoh = stop.elapsedTime(start) * 1e-3;

    std::cout << "HtoD: ";
    report(runtime_htod, DATA_SIZE);
    std::cout << ", DtoH: ";
    report(runtime_dtoh, DATA_SIZE);
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

cxxopts::ParseResult parse_options(int argc, const char *argv[]) {
  try {
    cxxopts::Options options(argv[0], "PCI-e bandwidth test");

    options.add_options()("d,device", "Device number",
                          cxxopts::value<int>()->default_value("0"))(
        "r,repetitions", "Number of repetitions",
        cxxopts::value<int>()->default_value("4"))("h,help", "Print usage");

    auto result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(EXIT_SUCCESS);
    }

    return result;
  } catch (const cxxopts::exceptions::exception &e) {
    std::cerr << "Error parsing command-line options: " << e.what()
              << std::endl;

    exit(EXIT_FAILURE);
  }
}

int main(int argc, const char *argv[]) {
  auto options = parse_options(argc, argv);
  const int device_id = options["device"].as<int>();
  const int nr_repetitions = options["repetitions"].as<int>();

  // Initialize runtime
  std::cout << ">> Initialize runtime" << std::endl;
  cu::init();

  // Initialize device
  std::cout << ">> Initialize device: " << device_id << std::endl;
  cu::Device device(device_id);

  // Initialize context
  std::cout << ">> Initialize context" << std::endl;
  cu::Context context(CU_CTX_SCHED_BLOCKING_SYNC, device);

  // Initialize stream
  std::cout << ">> Initialize stream" << std::endl;
  cu::Stream stream;

  // Allocate data
  std::cout << ">> Allocate data: " << DATA_SIZE / (1024 * 1e6) << " Gb"
            << std::endl;
  float *data = reinterpret_cast<float *>(malloc(DATA_SIZE));

  // Allocate device memory
  std::cout << ">> Allocate device memory" << std::endl;
  cu::DeviceMemory d_data(DATA_SIZE);

  // Print device info
  std::cout << std::endl << device;

  // Benchmark
  cu::Event start, stop;
  std::cout << std::setprecision(3) << std::setfill(' ');

  // Copy data malloc
  copy_data("malloc", stream, d_data, data, nr_repetitions);

  // Copy data cuMemHostAlloc
  cu::HostMemory h_data_allocated(DATA_SIZE);
  copy_data("cuMemHostAlloc", stream, d_data, h_data_allocated, nr_repetitions);

  // Copy data mapped
  cu::HostMemory h_data_writecombined(DATA_SIZE, CU_MEMHOSTALLOC_WRITECOMBINED);
  copy_data("cuMemHostAlloc (write combined)", stream, d_data,
            h_data_writecombined, nr_repetitions);

  // Copy data cuMemHostRegister
  cu::HostMemory h_data_registerd(data, DATA_SIZE);
  copy_data("cuMemHostRegister", stream, d_data, h_data_registerd,
            nr_repetitions);

  // Prefetch data cuMemAllocManaged
  cu::DeviceMemory d_data_unified(DATA_SIZE, CU_MEMORYTYPE_UNIFIED,
                                  CU_MEM_ATTACH_GLOBAL);
  memset(static_cast<void *>(d_data_unified), 1, DATA_SIZE);
  if (device.getAttribute<CU_DEVICE_ATTRIBUTE_CONCURRENT_MANAGED_ACCESS>()) {
    prefetch_data("cuMemAllocManaged", device, stream, d_data_unified,
                  nr_repetitions);
  }
  memset(h_data_allocated, 1, DATA_SIZE);

  // Copy data cuMemAllocManaged
  copy_data("cuMemAllocManaged", stream, d_data_unified, h_data_allocated,
            nr_repetitions);

  // Free data
  free(data);

  return EXIT_SUCCESS;
}
